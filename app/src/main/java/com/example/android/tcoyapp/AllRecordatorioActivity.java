package com.example.android.tcoyapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.android.tcoyapp.adapter.*;
import com.example.android.tcoyapp.model.Recordatorio;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Objects;

public class AllRecordatorioActivity extends AppCompatActivity {
    private RecordatorioAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ArrayList<Recordatorio> mRecordatoriosList = new ArrayList<Recordatorio>();


    FirebaseAuth usuario;
    DatabaseReference dataBase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_recordatorio);

        usuario = FirebaseAuth.getInstance();
        dataBase = FirebaseDatabase.getInstance().getReference();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerId);

       mRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
       // mRecyclerView.setLayoutManager(new GridLayoutManager(this,2));


        getRecordatoriosFromFirebase();

    }

    // Menú a mostrar en el xml asociado
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.icon_recordatorio: {
                Toast.makeText(this, "Ya estás aquí :)", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(AllRecordatorioActivity.this, AllRecordatorioActivity.class));
                break;
            }
            case R.id.icon_add: {
                //Toast.makeText(this, "agregar", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(AllRecordatorioActivity.this, RecordatorioActivity.class));
                break;
            }
            case R.id.icon_back: {
                Toast.makeText(this, "regresar", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(AllRecordatorioActivity.this, ProfileActivity.class));

                break;
            }
            default: break;

        }
        return true;
    }

    private void getRecordatoriosFromFirebase(){
        String id = Objects.requireNonNull(usuario.getCurrentUser()).getUid();
        dataBase.child("Usuario").child(id).child("Recordatorios").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){

                    for(DataSnapshot ds:dataSnapshot.getChildren()){
                        String Codigo= ds.getKey().toString();
                        String Denominacion= ds.child("titulo").getValue().toString();
                        String TipoCat="CF"; //(CF/CA)
                        String CodCuidado = "";
                        String mensaje= ds.child("descripción").getValue().toString();
                        String HoraInicio=ds.child("hora_fin").getValue().toString();
                        String HoraFin =ds.child("hora_inicio").getValue().toString();
                        //int DiasSemana [] = {0,0,0,0,0,0,0};
                        String FechaCreacion= ds.child("Fecha_creacion").getValue().toString();
                        boolean EstRegistro=false;
                        if(ds.child("estado_registro").getValue().toString().equals("true")){
                            EstRegistro = true;
                        } //(A,I,*)

                        mRecordatoriosList.add(new Recordatorio(Codigo,Denominacion,TipoCat,CodCuidado,mensaje,HoraInicio,HoraFin,FechaCreacion));

                    }

                    mAdapter = new RecordatorioAdapter(mRecordatoriosList,R.layout.recordatorio_list);

                    mRecyclerView.setAdapter(mAdapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}