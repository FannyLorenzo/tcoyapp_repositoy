package com.example.android.tcoyapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.tcoyapp.R;
import com.example.android.tcoyapp.model.Recordatorio;

import java.util.ArrayList;

public class RecordatorioAdapter extends RecyclerView.Adapter<RecordatorioAdapter.ViewHolder> {
    private ArrayList<Recordatorio> recordatoriosList;
    private int resource;

    public RecordatorioAdapter(ArrayList<Recordatorio> recordatoriosList, int resource) {

        this.recordatoriosList = recordatoriosList;
        this.resource = resource;
    }

    @NonNull
    @Override // enlaza el adaptador con la list
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View  view = LayoutInflater.from(viewGroup.getContext()).inflate(resource,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int index) {
        Recordatorio recordatorio = recordatoriosList.get(index);
        holder.txt_titulo.setText(recordatorio.getDenominacion());
        holder.txt_descripcion.setText(recordatorio.getMensaje());
       // holder.txt_tipo_cuidado.setText(recordatorio.getTipoCat());
        //holder.txt_cuidado.setText(recordatorio.getCodCuidado());
        holder.txt_horaIni.setText(recordatorio.getHoraInicio());
        holder.txt_horafin.setText(recordatorio.getHoraFin());

    }

    @Override
    public int getItemCount() {
        return recordatoriosList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_titulo;
        private TextView txt_descripcion;
        private TextView txt_horaIni;
        private TextView txt_horafin;
        private TextView txt_tipo_cuidado;
        private TextView txt_cuidado;

        private Button btn_ver;
        private Button btn_eliminar;
        private Button btn_modificar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_titulo = (TextView) itemView.findViewById(R.id.id_titulo);
            txt_descripcion = (TextView) itemView.findViewById(R.id.id_descripcion);
            txt_horaIni = (TextView) itemView.findViewById(R.id.id_hora_ini);
            txt_horafin = (TextView) itemView.findViewById(R.id.id_hora_fin);

        }

    }
}



