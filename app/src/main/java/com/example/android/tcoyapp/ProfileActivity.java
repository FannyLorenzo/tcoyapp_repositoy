package com.example.android.tcoyapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ProfileActivity extends AppCompatActivity {

    private Button btn_CerrarSesion;
    private Button btn_Modificar;
    private TextView txt_miname;
    private TextView txt_miemail;
    private ImageView imageView;

    FirebaseAuth usuario;
    DatabaseReference dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        usuario = FirebaseAuth.getInstance();
        dataBase = FirebaseDatabase.getInstance().getReference();

        txt_miname = (TextView) findViewById(R.id.edit_Name);
        txt_miemail = (TextView) findViewById(R.id.edit_Email);
        imageView = (ImageView) findViewById(R.id.imageView);

        // modificar datos de usuario
        btn_Modificar = (Button) findViewById(R.id.btn_Modificar);
       btn_Modificar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               startActivity(new Intent(ProfileActivity.this, modificarUsuarioActivity.class));
               //finish();
           }
       });

        // Cerrar sesión de usuario

    btn_CerrarSesion = (Button) findViewById(R.id.btn_CerrarSesion);
    btn_CerrarSesion.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            usuario.signOut();
            startActivity(new Intent(ProfileActivity.this, MainActivity.class));
            finish(); // para que no pueda volver hacia atras cuando cierre sesión
        }
    });

        //para mostrar datos de uusario actual en Profile
    getUSerInfo();

    }

    private void getUSerInfo(){
        String id = Objects.requireNonNull(usuario.getCurrentUser()).getUid();

        dataBase.child("Usuario").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("name").getValue().toString();
                String email = dataSnapshot.child("email").getValue().toString();
                Uri photo = usuario.getInstance().getCurrentUser().getPhotoUrl();
                txt_miname.setText(name);
                txt_miemail.setText(email);
                Glide.with(ProfileActivity.this).load(String.valueOf(photo)).into(imageView);
              //  imageView.setImageURI(photo);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // Menú a mostrar en el xml asociado
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.icon_recordatorio: {
                Toast.makeText(this, "recordatorio", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ProfileActivity.this, AllRecordatorioActivity.class));
                break;
            }
            case R.id.icon_add: {
                //Toast.makeText(this, "agregar", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ProfileActivity.this, RecordatorioActivity.class));
                break;
            }
            case R.id.icon_back: {
                Toast.makeText(this, "regresar", Toast.LENGTH_SHORT).show();
                break;
            }
            default: break;

        }



        return true;
    }
}