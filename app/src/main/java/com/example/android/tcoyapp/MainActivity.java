package com.example.android.tcoyapp;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.widget.Toast.*;

public class MainActivity extends AppCompatActivity {

    /** Variables widget**/
    private EditText miname;
    private EditText miemail;
    private EditText mipassword;
    private Button btn_registrar;
    private  Button btn_sendToLogin;
    private CheckBox miterminos;


    /** Variables de datos**/

    private String name = "";
    private String email = "";
    private String password = "";
    private boolean terminos = false;  //aquiiii

    FirebaseAuth usuario;
    DatabaseReference dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /** Variables Registrar -Widget
         * */
        usuario = FirebaseAuth.getInstance();
        dataBase = FirebaseDatabase.getInstance().getReference();

        miname = (EditText) findViewById(R.id.edit_Name);
        miemail = (EditText) findViewById(R.id.edit_Email);
        mipassword = (EditText) findViewById(R.id.edit_password);
        btn_registrar = (Button) findViewById(R.id.btn_Registrar);
        btn_sendToLogin = (Button) findViewById(R.id.btn_sendToLogin);
        miterminos = (CheckBox) findViewById(R.id.checkBox_terminos);  //aquiiii

        btn_registrar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

            name = miname.getText().toString();
            email = miemail.getText().toString();
            password = mipassword.getText().toString();
            terminos = miterminos.isChecked();

           // var_terminos = terminos.didTouchFocusSelect(); // creo q es eso, si no gg

                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {  // aqui lo de los terminos
                    if (password.length() >= 6) {
                        if(terminos) {
                            registerUser();
                        }else{
                            Toast.makeText(MainActivity.this, "Acepte los términos y condiciones", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(MainActivity.this, "La contraseña debe tener al menos 6 caracteres", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(MainActivity.this, "Debe completar los campos", LENGTH_SHORT).show();
                }



            }
        });

        btn_sendToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });

    }

    private void registerUser() {

         usuario.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    //map
                    Map<String, Object> map = new HashMap<>();
                    map.put("name", name);
                    map.put("email", email);
                    map.put("password", password);

                    // variables de id
                    String id = Objects.requireNonNull(usuario.getCurrentUser()).getUid();

                    // creacion de usuario en la base de datos
                    dataBase.child("Usuario").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {
                        if(task2.isSuccessful()){
                            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                        finish();
                        }else{
                            Toast.makeText(MainActivity.this, "No se pudieron crear los datos correctamente", LENGTH_SHORT).show();
                        }
                        }
                    });
                }else{
                    Toast.makeText(MainActivity.this, "No se pudo completar regitrar al usuario", LENGTH_SHORT).show();



                }
            }
        });

    }
@Override
    protected  void onStart(){
        super.onStart();

        if(usuario.getCurrentUser()!=null){ // significa q el usuario ya inició sesión
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            finish();
        }else{

        }

    }


}
