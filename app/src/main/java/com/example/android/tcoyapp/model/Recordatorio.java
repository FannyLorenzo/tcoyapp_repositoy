package com.example.android.tcoyapp.model;

import java.sql.Time;
import java.util.Arrays;
import java.util.Date;

public class Recordatorio {

    String Codigo;
    String Denominacion;
    String TipoCat; //(CF/CA)
    String CodCuidado;
    String mensaje;
    String HoraInicio;
    String HoraFin;
    //int DiasSemana [] = {0,0,0,0,0,0,0};
    String FechaCreacion;    //
    boolean EstRegistro; //(A,I,*)

    // recordatorio
    Recordatorio(){}

    public Recordatorio(String codigo, String denominacion, String tipoCat, String codCuidado, String mensaje, String horaInicio, String horaFin, String fechaCreacion) {
        Codigo = codigo;
        Denominacion = denominacion;
        TipoCat = tipoCat;
        CodCuidado = codCuidado;
        this.mensaje = mensaje;
        HoraInicio = horaInicio;
        HoraFin = horaFin;
        FechaCreacion = fechaCreacion;
        EstRegistro = true;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDenominacion() {
        return Denominacion;
    }

    public void setDenominacion(String denominacion) {
        Denominacion = denominacion;
    }

    public String getTipoCat() {
        return TipoCat;
    }

    public void setTipoCat(String tipoCat) {
        TipoCat = tipoCat;
    }

    public String getCodCuidado() {
        return CodCuidado;
    }

    public void setCodCuidado(String codCuidado) {
        CodCuidado = codCuidado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getHoraInicio() {
        return HoraInicio;
    }

    public void setHoraInicio(String horaInicio) {
        HoraInicio = horaInicio;
    }

    public String getHoraFin() {
        return HoraFin;
    }

    public void setHoraFin(String horaFin) {
        HoraFin = horaFin;
    }

    public String getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }

    public boolean getEstRegistro() {
        return EstRegistro;
    }

    public void setEstRegistro(boolean estRegistro) {
        EstRegistro = estRegistro;
    }
}
