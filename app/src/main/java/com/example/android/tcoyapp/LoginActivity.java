package com.example.android.tcoyapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {

    private EditText miemail;
    private EditText mipassword;
    private Button btn_iniciarSesion;


    private String email = "";
    private String password = "";

    FirebaseAuth usuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usuario = FirebaseAuth.getInstance();

        miemail = (EditText) findViewById(R.id.edit_Email);
        mipassword = (EditText) findViewById(R.id.edit_password);
        btn_iniciarSesion = (Button) findViewById(R.id.btn_IniciarSesion);

        // botonn de iniciar sesion
        btn_iniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = miemail.getText().toString();
                password = mipassword.getText().toString();

                if(!email.isEmpty() && !email.isEmpty()){
                    loginUser();
                }else{

                }

            }
        });




    }

    private void loginUser() {

        usuario.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
                }else{
                    Toast.makeText(LoginActivity.this, "No se pudo iniciar sesión, porfavor comprueba los datos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}