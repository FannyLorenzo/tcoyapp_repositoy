package com.example.android.tcoyapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.sip.SipSession;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.widget.Toast.LENGTH_SHORT;

public class RecordatorioActivity extends AppCompatActivity {

    private EditText edit_titulo;
    private EditText edit_descripcion;
    private TextView txt_horaIni;
    private TextView txt_horafin;
    private Button btn_time_ini;
    private Button btn_time_fin;
    private Spinner spi_tipo_cuidado;
    private Spinner spi_cuidado;
    private Button btn_crearRecordatorio;

  //  atributos model - int Codigo;
    private String Denominacion="";
    private String TipoCat="CF"; //(CF/CA)
    private String CodCuidado="";
    private String mensaje="";
    private String HoraInicio;
    private String HoraFin;
    private int DiasSemana [] = {0,0,0,0,0,0,0};
    private String FechaCreacion;    //
    private boolean EstRegistro=false; //(A,I,*)

    Context mContext=this;
    FirebaseAuth usuario;
    DatabaseReference dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordatorio);

        usuario = FirebaseAuth.getInstance();
        dataBase = FirebaseDatabase.getInstance().getReference();

        edit_titulo = (EditText) findViewById(R.id.edit_Titulo);
        edit_descripcion = (EditText) findViewById(R.id.edit_Descripcion);
        btn_time_ini = (Button) findViewById(R.id.btn_hora_ini);
        btn_time_fin = (Button) findViewById(R.id.btn_hora_fin);
        txt_horaIni = (TextView) findViewById(R.id.edit_HoraInicio);
        txt_horafin = (TextView) findViewById(R.id.edit_HoraFin);
        spi_tipo_cuidado = (Spinner) findViewById(R.id.tipo_cuidado_select);
        spi_cuidado = (Spinner) findViewById(R.id.cuidado_select);

        btn_crearRecordatorio = (Button) findViewById(R.id.btn_Guardar);

        Calendar calendar = Calendar.getInstance();
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);

btn_time_ini.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        TimePickerDialog time1 = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                txt_horaIni.setText(hourOfDay+" : "+minute);
            }
        }, hour,minute,android.text.format.DateFormat.is24HourFormat(mContext));
        time1.show();
    }
});
        btn_time_fin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog time2 = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        txt_horafin.setText(hourOfDay+" : "+minute);
                    }
                }, hour,minute,android.text.format.DateFormat.is24HourFormat(mContext));
                time2.show();
            }
        });

        btn_crearRecordatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Denominacion = edit_titulo.getText().toString();
                mensaje = edit_descripcion.getText().toString();
                HoraInicio = txt_horaIni.getText().toString();
                HoraFin = txt_horafin.getText().toString();
                FechaCreacion = (new Date()).toString();
                EstRegistro =true;


                if(!Denominacion.isEmpty() && !mensaje.isEmpty()){
                    registerRecordatorio();

                }else{

                }

            }
        });






    }

    private void registerRecordatorio() {

        String id = Objects.requireNonNull(usuario.getCurrentUser()).getUid();
        System.out.println(" cual es el id "+ id);
        //map
        Map<String, Object> map = new HashMap<>();
        map.put("titulo", Denominacion);
        map.put("descripción", mensaje);
        map.put("hora_inicio", HoraInicio);
        map.put("hora_fin", HoraFin);
        map.put("Fecha_creacion", FechaCreacion);
        map.put("estado_registro", EstRegistro);


        // creacion de usuario en la base de datos
        dataBase.child("Usuario").child(id).child("Recordatorios").push().setValue(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(RecordatorioActivity.this, "El recordatorio se ha creado correctamente", LENGTH_SHORT).show();
                startActivity(new Intent(RecordatorioActivity.this, AllRecordatorioActivity.class));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(RecordatorioActivity.this, "Hubo un error al tratar de guardar los datos", LENGTH_SHORT).show();

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.icon_recordatorio: {
                Toast.makeText(this, "recordatorio", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RecordatorioActivity.this, AllRecordatorioActivity.class));
                break;
            }
            case R.id.icon_add: {
                Toast.makeText(this, "Ya estás en agregar recordatorio", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(RecordatorioActivity.this, RecordatorioActivity.class));
                break;
            }
            case R.id.icon_back: {
                //Toast.makeText(this, "regresar", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RecordatorioActivity.this, AllRecordatorioActivity.class));
                break;
            }
            default: break;

        }



        return true;
    }


}