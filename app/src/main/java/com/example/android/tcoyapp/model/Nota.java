package com.example.android.tcoyapp.model;

import java.util.Date;

public class Nota {

    int Codigo;
    String Nombre="";
    String Contenido="";
    Date FechaCreacion;

    public Nota(int codigo) {
        Codigo = codigo;
        Date FechaCreacion = new Date();
    }

    public Nota(int codigo, String nombre, String contenido) {
        Codigo = codigo;
        Nombre = nombre;
        Contenido = contenido;
        FechaCreacion =  new Date();
    }

    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int codigo) {
        Codigo = codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getContenido() {
        return Contenido;
    }

    public void setContenido(String contenido) {
        Contenido = contenido;
    }

    public Date getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }

    @Override
    public String toString() {
        return "Nota{" +
                "Codigo=" + Codigo +
                ", Nombre='" + Nombre + '\'' +
                ", Contenido='" + Contenido + '\'' +
                ", FechaCreacion=" + FechaCreacion +
                '}';
    }
}
