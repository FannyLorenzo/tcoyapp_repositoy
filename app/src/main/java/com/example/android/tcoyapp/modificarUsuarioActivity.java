package com.example.android.tcoyapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


import static android.widget.Toast.LENGTH_SHORT;

public class modificarUsuarioActivity extends AppCompatActivity {
    private Button btn_Guardar;
    private Button btn_Cancelar;
    private EditText miname;
    private EditText miemail;
    private EditText mipassword;
    String name="";
    String password="";

    FirebaseAuth usuario;
    DatabaseReference dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_usuario);

        miname = (EditText) findViewById(R.id.edit_Name);
        mipassword = (EditText) findViewById(R.id.edit_password);
        btn_Guardar = (Button) findViewById(R.id.btn_Guardar);
        btn_Cancelar = (Button) findViewById(R.id.btn_cancelar);

        //mostrar los datos que ya estaban


        btn_Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                usuario = FirebaseAuth.getInstance();
                dataBase = FirebaseDatabase.getInstance().getReference();

                name = miname.getText().toString();
                password = mipassword.getText().toString();

                // var_terminos = terminos.didTouchFocusSelect(); // creo q es eso, si no gg

                if (!name.isEmpty() && !password.isEmpty()) {  // aqui lo de los terminos
                    if (password.length() >= 6) {

                            updateUser();

                    }
                    else {
                        Toast.makeText(modificarUsuarioActivity.this, "La contraseña debe tener al menos 6 caracteres", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(modificarUsuarioActivity.this, "Debe completar los campos", LENGTH_SHORT).show();
                }
            }
        });

        btn_Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(modificarUsuarioActivity.this, ProfileActivity.class));
            }
        });

getDatos();

    }

    private void getDatos() {
        usuario = FirebaseAuth.getInstance();
        dataBase = FirebaseDatabase.getInstance().getReference();
        String id = Objects.requireNonNull(usuario.getCurrentUser()).getUid();
        System.out.println(" cual es el id "+ id);

        dataBase.child("Usuario").child(id).addValueEventListener(new ValueEventListener() {
            @Override

            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String txt_name = dataSnapshot.child("name").getValue().toString();
                String txt_password = dataSnapshot.child("password").getValue().toString();
                miname.setText(txt_name);
                mipassword.setText(txt_password);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateUser() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("password", password);

        // variables de id

        String id = Objects.requireNonNull(usuario.getCurrentUser()).getUid();

        // creacion de usuario en la base de datos
        dataBase.child("Usuario").child(id).updateChildren(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(modificarUsuarioActivity.this, "Los datos se han actualizado correctamente", LENGTH_SHORT).show();
                startActivity(new Intent(modificarUsuarioActivity.this, ProfileActivity.class));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(modificarUsuarioActivity.this, "HUbo un error al tratar de actualizar los datos", LENGTH_SHORT).show();
            }
        });
    }
}